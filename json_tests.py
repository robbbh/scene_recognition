#!/usr/bin/python

import json

def main():

    key = []
    records = {}
    record_set = []

    # trying to get {"key": ("amber", "gold", "ruby")}

    try:
        with open("objects.json") as object_data:
            data = json.load(object_data)

        # gets the tag values from json and stores them as keys
        key = [record["tag"] for record in data["objects"]]
        # fills a dict up with the found keys
        value_dict = dict((el,[]) for el in key)

        # gets the record tag and object_name into an array
        for record in data["objects"]:
            record_set.append([record["tag"], record["object_name"]])
        # populates the dictionary with a key representing objects
        for objects in record_set:
            if str(objects[0]) in value_dict:
                value_dict[str(objects[0])].append(str(objects[1]))

    except Exception as e:
        print str(e)


if __name__ == "__main__":
    main()