import numpy as np
import cv2
from matplotlib import pyplot as plt
from os import listdir
from os.path import isfile, join

def main(query_image, train_image):
    # This variable should be dynamic based on the train image
    MIN_MATCH_COUNT = 5

    img1 = cv2.imread(query_image,0)          # queryImage
    img2 = cv2.imread(train_image,0) # trainImage

    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    matches = flann.knnMatch(des1,des2,k=2)
    matchesMask = [[0,0] for i in xrange(len(matches))]
    # store all the good matches as per Lowe's ratio test.
    good = []
    distances = []

    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)
            #print m.distance

    """for i,(m,n) in enumerate(matches):
        if m.distance < 0.8*n.distance:
            matchesMask[i]=[1,0]"""

    if len(good)>MIN_MATCH_COUNT:
        match_array = [matches.distance for matches in good]
        # gets 20 best matches 
        matches = sorted(match_array, key = lambda x:x)
        # returns the image names, number of matches and average of top 20 matches
        print "query_image: {0} \tvs\t train_image: {1}\t\t (matches, distance) = ({2}, {3})".format(query_image, train_image, len(good), sum(matches[:20])/20)

    """draw_params = dict(matchColor = (0,255,0),
                    singlePointColor = (255,0,0),
                    matchesMask = matchesMask,
                    flags = 0)

    img3 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,matches,None,**draw_params)

    plt.imshow(img3,),plt.show()"""
        

if __name__ == "__main__":

    # test_scene = [f for f in listdir('./scenes') if isfile(join('./scenes', f))]  
    # train_scene = 'received_scenes/test_image_orange.jpg'

    #train_scene = [f for f in listdir('./objects') if isfile(join('./objects', f))]
    test_scene = ["lion_room_lemon_ape.JPG"]
    # train_scene = ["alt_apple.jpg", "apple.jpg", "alt_apple_2.jpg"]
    # train_scene = ["blue_hat.jpg", "blue_hat_alt.jpg", "blue_hat_alt_cheat.jpg"]
    train_scene = ["lemon.jpg"]#, "yellow_hat_altern.jpg"]

    for train in train_scene:
        print "\n"
        for test in test_scene:
            main("scenes/" + test, "objects/" + train)