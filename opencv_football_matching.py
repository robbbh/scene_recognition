#!/usr/bin/python

import numpy as np
import cv2
from matplotlib import pyplot as plt
from os import listdir
from os.path import isfile, join


def main(test, train):

    print "Testing Image: {0}".format(test)
    print "Training Image: {0}".format(train)
    


    img1 = cv2.imread(test,0) # queryImage
    img2 = cv2.imread(train,0) # trainImage

    # Initiate SIFT detector
    orb = cv2.ORB_create(scoreType=cv2.ORB_FAST_SCORE,WTA_K = 3)

    # find the keypoints and descriptors with SIFT
    kp1, des1 = orb.detectAndCompute(img1,None)
    kp2, des2 = orb.detectAndCompute(img2,None)

    # create BFMatcher object
    bf = cv2.BFMatcher(cv2.NORM_HAMMING2, crossCheck=True)

    # Match descriptors.
    matches = bf.match(des1,des2)

    match_sum = []

    for match in matches:
        # print match.distance
        match_sum.append(match.distance)

    matching_sum = np.sum(match_sum)
    matching_num = len(match_sum)

    #print matching_num
    # print matching_sum

    # Sort them in the order of their distance.

    matches = sorted(matches, key = lambda x:x.distance)

    max_dist = 0.0
    match_array = []

    for match in matches[:20]:
        # print match.distance
        match_array.append(match.distance)

    print sum(match_array) / 20

    

    # Draw first 10 matches.
    #img3 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,flags=2)
    #img3 = cv2.drawMatches(img1,kp1,img2,kp2,matches[:10],None, flags=2)

    #plt.imshow(img3)
    #plt.show()

    #if matching_sum > 5000.0:
        #print "No Game Object Recognised."



if __name__ == "__main__":


    test_scene = [f for f in listdir('./scenes') if isfile(join('./scenes', f))]  
    # train_scene = 'received_scenes/test_image_orange.jpg'

    train_scene = [f for f in listdir('./objects') if isfile(join('./objects', f))]  
    
    for train in train_scene:
        main(test_scene, "objects/" + train)