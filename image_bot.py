#!usr/bin/python
import telepot
import time
import sys
import numpy as np
import cv2
from matplotlib import pyplot as plt
import os
import operator

class Bot_com:

    def __init__(self):
        # image bot key
        self.bot = telepot.Bot('353119581:AAGUpjclZ2RjWW-L-OrHU4bTHltqB1SGYFc')
        self.bot.message_loop(self.handle)
        self.trusted = ["TheMariday","robbbh"]
        self.image_directory = "received_scenes"
        self.current_directory = os.path.dirname(os.path.realpath(__file__))
        self.download_file_directory = os.path.join(self.current_directory, self.image_directory) 


    def handle(self, msg):
        # print msg
        self.userName = msg["from"]["username"]
        if self.userName not in self.trusted:
            self.bot.sendMessage(self.chat_id, "Unverified User...\nCyka Blyat.")
            return

        else:
            self.chat_id = msg['chat']['id']
            self.check_message_type(msg, self.userName)


    def check_message_type(self, msg, username):
        self.chat_id = msg['chat']['id']

        if "photo" in msg:
            # gets file id of the largest resolution image
            self.photoId = msg["photo"][-1]["file_id"]
            print "Received photo ID: {0}".format(self.photoId)          
        else:
            print "Please Give Photo Input."
            self.bot.sendMessage(self.chat_id, "Please Give Photo Input.")
            return

           
        self.parse_path(self.photoId, username)


    def parse_path(self, photoId, username):
        #try:
        if photoId:
            self.filename = "{0}-{1}.JPG".format(username, self.chat_id)
            self.file_path = os.path.join(self.download_file_directory, self.filename)
            self.bot.download_file(photoId, self.file_path)
            self.bot.sendMessage(self.chat_id, "Image Received Successfully.\nFile name is {0}.\nAnalysing Image.".format(self.filename))  

            image_rec = opencv_orb(self.file_path, self.chat_id)
            image_rec.compare_against_training_images()

        #except Exception:
         #   print "Error Parsing the image."
          #  self.bot.sendMessage(self.chat_id, "Error Parsing the Image.") 


    def run(self):
        while True:
            time.sleep(1)


class opencv_orb:

    def __init__(self, test_image_path, chat_id):
        """ Apply's the ORB algoriothm for image classification. """

        self.bot = telepot.Bot('353119581:AAGUpjclZ2RjWW-L-OrHU4bTHltqB1SGYFc')
        self.train_scene_direc = "scenes/"

        self.train_scene = ['elephant_orange.JPG', 'elephant_orange_room.JPG', 'elephant_tennis_greenhat.JPG', 'giraffe_banana_redhat.JPG',
                 'giraffe_room_redhat_robot.JPG', 'giraffe_rugby.JPG', 'hippo_pineapple.JPG', 'hippo_room_bluehat.JPG', 'hippo_soccer.JPG',
                 'lion_golf.JPG', 'lion_pinkhat.JPG', 'lion_room_lemon_ape.JPG', 'tiger_basketball_yellowhat.JPG',
                 'tiger_pear.JPG', 'tiger_pear_green_baloon.JPG', 'zebra_apple_purplehat.JPG', 'zebra_apple_room_ghost.JPG', 
                 'zebra_cricket.JPG'
                 ]

        self.train_scene_dict_with_CNL = {
            'elephant_orange.JPG': [("The Elephant eats the Orange."),
                                    ("Mae Eliffant yn bwyta Oren.")], 
            'elephant_orange_room.JPG': [("The Elephant eats the Orange.", "The Eliphant is in the Ruby room."),
                                    ("Mae Eliffant yn bwyta Oren.", "Mae Eliffant yn yr ystafell Ruby.")], 
            'elephant_tennis_greenhat.JPG': [("The Elephant plays Tennis.", "The Elephant wears a Green hat."),
                                    ("Mae Eliffant yn chwarae tenis.", "Mae Eliffant yn giwsgo het Gwyrdd.")], 
            'giraffe_banana_redhat.JPG': [("The Giraffe eats the Bananas.", "The Giraffe wears a Red hat."),
                                    ("Mae jiraff yn bwyta Bananas.", "Mae jiraff yn gwisgo het Coch.")],
            'giraffe_room_redhat_robot.JPG': [("The Giraffe is in the Saphire room.", "The Giraffe wears a Red hat.", "The Robot is in the Saphire Room."),
                                    ("Mae jiraff yn yr ystafell Saphire.", "Mae jiraff yn gwisgo het Coch.", "Mae robot yn yr ystafell Saphire.")], 
            'giraffe_rugby.JPG': [("The Giraffe plays Rugby."),
                                    ("Mae jiraff yn chwarae Rygbi.")], 
            'hippo_pineapple.JPG': [("The Hippo eats the Pineapple."),
                                    ("Mae Hippo yn bwyta pin-afal.")], 
            'hippo_room_bluehat.JPG': [("The Hippo is in the Gold room.", "The Hippo wears a Blue hat."),
                                    ("Mae Hippo yn yr ystafell Aur.", "Mae Hippo yn gwisgo het Glas.")], 
            'hippo_soccer.JPG': [("The Hippo plays Soccer."),
                                    ("Mae Hippo yn Chwarae Pel-droed.")],
            'lion_golf.JPG': [("The Lion plays Golf."),
                                    ("Mae Llew yn chwarae Golff.")],  
            'lion_pinkhat.JPG': [("The Lion wears a Pink hat."),
                                    ("Mae Llew yn gwisgo het Pinc.")], 
            'lion_room_lemon_ape.JPG': [("The Lion is in the Amber room.", "The Lion eats the Lemon.", "The Gorilla is in the Amber room."),
                                    ("Mae Llew yn yr ystafell Amber.", "Mae Llew yn bwyta Lemon.", "Mae Gorila yn yr ystafell Amber.")], 
            'tiger_basketball_yellowhat.JPG': [("The Tiger plays Basketball.", "The Tiger wears a yellow hat."),
                                    ("Mae Teigr yn chwarae Pel-Fasged.", "Mae Teigr yn gwisgo het Melyn.")],
            'tiger_pear.JPG': [("The Tiger eats the Pear."),("Mae Teigr yn bwyta Gellygen.")], 
            'tiger_pear_green_baloon.JPG': [("The Tiger is in the Emerald room.", "The Tiger eats the Pear.", "The balloon is in the Emerald room."),
                                    ("Mae Teigr yn yr ystafell Emerald.", "Mae Teigr yn bwyta Gellygen.", "Mae'r Balwn yn yr ystafell Emerald.")], 
            'zebra_apple_purplehat.JPG': [("The Zebra eats the Apple.", "The Zebra wears a Purple hat."),
                                    ("Mae'r Sebra yn bwyta Afal.", "Mae'r Sebra yn gwisgo het Porffor.")], 
            'zebra_apple_room_ghost.JPG': [("The Zebra is in the Silver room.", "The Zebra eats the Apple.", "The Ghost is in the Silver Room."),
                                    ("Mae Sebra yn yr ystafell Silver.", "Mae Sebra yn bwyta Afal.", "Mae'r Ysbryd yn yr ystafell Silver.")], 
            'zebra_cricket.JPG': [("The Zebra plays Cricket."),
                                    ("Mae Sebra yn chwarae Criced.")]
        }


        self.test_scene = test_image_path
        self.chat_id = chat_id


    def compare_against_training_images(self):
        distance_with_file_array = []

        for train in self.train_scene:
            distance_with_file_array.append(self.test_image(self.test_scene, self.train_scene_direc + train))

        # sorts the tuples by their value (lowest to highest) 
        self.distance_sorted = sorted(distance_with_file_array, key=operator.itemgetter(1))

        # Returns top 3 matches to bot
        #self.bot.sendMessage(self.chat_id, "Closest Match = {0}\n2nd Closest Match = {1}\
        #\n3rd Closest Match = {2}".format(self.distance_sorted[0], self.distance_sorted[1], self.distance_sorted[2]))
        # Prints top 3 matches to terminal
        #print "Closest Match = {0}\n2nd Closest Match = {1}\
        #    \n3rd Closest Match = {2}".format(self.distance_sorted[0], self.distance_sorted[1], self.distance_sorted[2])

        self.return_cnl_string(self.distance_sorted)


    def return_cnl_string(self, distance_tuple):
        """ Sends the BOT the interpretted image in a CNL format """

        file_match = distance_tuple[0][0]
        cnl_location = self.train_scene_dict_with_CNL[file_match][:]
        cnl_keys = self.train_scene_dict_with_CNL.keys()

        if file_match in [name for name in cnl_keys]:
            for cnl in cnl_location[:]:
                if len(cnl) > 5:
                    print cnl
                    self.bot.sendMessage(self.chat_id, cnl)
                else:
                     for language in cnl:
                        print language
                        self.bot.sendMessage(self.chat_id, language)   


    def test_image(self, test, train):

        self.img1 = test
        self.img1 = cv2.imread(test,0) # queryImage
        self.img2 = cv2.imread(train,0) # trainImage
        # Initiate SIFT detector
        self.orb = cv2.ORB_create(scoreType=cv2.ORB_FAST_SCORE,WTA_K = 3)
        # find the keypoints and descriptors with SIFT
        self.kp1, self.des1 = self.orb.detectAndCompute(self.img1,None)
        self.kp2, self.des2 = self.orb.detectAndCompute(self.img2,None)
        # create BFMatcher object
        self.bf = cv2.BFMatcher(cv2.NORM_HAMMING2, crossCheck=True)
        # Match descriptors.
        matches = self.bf.match(self.des1,self.des2)

        return self.get_best_match(matches, test, train)


    def get_best_match(self, matches, test, train):

        # Sort them in the order of their distance.
        matches = sorted(matches, key = lambda x:x.distance)
        self.match_array = []

        # gets the top 20 matches and gets the average distance
        # In theory the lowest average distance should be the closest match
        for match in matches[:20]:
            self.match_array.append(match.distance)

        self.average_distances = sum(self.match_array) / 20

        # gets the file names only
        self.test_image_base = os.path.basename(test)
        self.train_image_base = os.path.basename(train)
        self.average_distances_print = "Test Image {0} vs Training image {1}\
        \n\tAverage Distance: {2}".format(self.test_image_base, self.train_image_base, self.average_distances)

        # print self.average_distances_print
        # self.bot.sendMessage(self.chat_id, self.average_distances_print)

        self.image_distance_tuple = (self.train_image_base, self.average_distances)
        #print self.image_distance_tuple

        return self.image_distance_tuple


    
if __name__ == "__main__":
    go = Bot_com()
    print "Image Bot Launched!"
    go.run()
