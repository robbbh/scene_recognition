#!usr/bin/python
import telepot
import time
import sys
import numpy as np
import cv2
import os
from os import listdir
from os.path import isfile, join, dirname, realpath
import json
import requests
import traceback

class telegram_handle:

    def __init__(self):
        # bot API key
        self.bot = telepot.Bot('353119581:AAGUpjclZ2RjWW-L-OrHU4bTHltqB1SGYFc')
        self.bot.message_loop(self.handle)
        self.trusted = ["robbbh"]
        


    def handle(self, msg):
        self.userName = msg["from"]["username"]
        self.chat_id = msg['chat']['id']

        if self.userName not in self.trusted:
            self.bot.sendMessage(self.chat_id, "Unverified User...\nPlease use a verified account.")
            return
        else:
            self.check_message_type(msg)


    def check_message_type(self, msg):

        if "text" in msg:
            self.message = msg['text'].encode('utf-8').strip()
            recieved_text = "Recieved text input = '{0}'".format(self.message)
            print recieved_text

            interpret_text = Linguistic_Module(self.chat_id)
            interpret_text.check_if_question_exists(str(self.message))

        else:
            error_message = "Messages must be in a text format, consisting of a SHERLOCK question."
            print error_message
            self.bot.sendMessage(self.chat_id, error_message)
            return


    def run(self):
        while True:
            time.sleep(1)


class Visual_Module:

    def __init__(self, chat_id):
        """ Apply's the SIFT algoriothm for object classification based off
             training images. """

        self.current_directory = dirname(realpath(__file__))
        self.bot = telepot.Bot('353119581:AAGUpjclZ2RjWW-L-OrHU4bTHltqB1SGYFc')
        self.train_scene_direc = "scenes/"
        self.train_obj_direc = "objects/"
        self.scenes_direc = join(self.current_directory, self.train_scene_direc)
        self.scenes = [f for f in listdir(self.scenes_direc) if isfile(join(self.scenes_direc, f))]
        self.chat_id = chat_id


    def find_scenes_with_objects(self, subject_data, object_data):
        """ Searches for Objects within scenes. """
        positive_scenes = []
        object_to_find = subject_data[0]
        maximum_distance = subject_data[1]
        minimum_matches = subject_data[2]
        input_image_name = subject_data[3]


        for scene in self.scenes:
            positive_scenes.append(self.test_image(self.train_scene_direc + scene, self.train_obj_direc + object_to_find, minimum_matches, maximum_distance, input_image_name))

        scenes_with_object = [i[0] for i in positive_scenes if i != None]

        if len(scenes_with_object) is 0:
            print "No Scenes found with object."
            self.bot.sendMessage(self.chat_id, "No Scenes found with object.")
        else:
            subject = self.find_objects_within_refined_scenes(scenes_with_object, object_data)
            return subject
        


    def find_objects_within_refined_scenes(self, scenes_with_object, object_data):
        """ Searches for objects within refined scenes. """
        answer_list = []

        for details in object_data:
            input_image = details[0]
            maximum_distance = details[1]
            minimum_matches = details[2]  
            input_image_name = details[3]

            for scene in scenes_with_object:
                answer_list.append(self.test_image(scene, self.train_obj_direc + input_image,  minimum_matches, maximum_distance, input_image_name))

        answer = "\n".join(set([i[1] for i in answer_list if i != None]))

        if len(answer) is 0:
            print "No Scenes found with object."
            self.bot.sendMessage(self.chat_id, "No objects found in scene.")
        else:
            return answer


    def test_image(self, test, train, min_match, max_dist, input_image_name):
        """ Performs image classification with the SIFT descriptor """
        # opens query and train iamge in grayscale
        query_image = cv2.imread(test,0)
        train_image = cv2.imread(train,0) 

        # Initiate SIFT detector
        sift = cv2.xfeatures2d.SIFT_create()

        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(query_image,None)
        kp2, des2 = sift.detectAndCompute(train_image,None)

        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
        search_params = dict(checks = 50)

        flann = cv2.FlannBasedMatcher(index_params, search_params)

        matches = flann.knnMatch(des1,des2,k=2)

        return self.get_best_match(matches, test, train, max_dist, min_match, input_image_name)       


    def get_best_match(self, matches, test, train, max_dist, min_match, input_image_name):
        """ Works out the best match based on training data. """

        # store all the good matches as per Lowe's ratio test.
        good = []
        for m,n in matches:
            if m.distance < 0.7*n.distance:
                good.append(m) 


        if len(good) >= int(min_match):
            match_array = [matches.distance for matches in good]
            # sorts matches by size
            matches = sorted(match_array, key = lambda x:x)
            # gets average of 20 best matches
            distance = sum(matches[:20])/20

            if distance <= int(max_dist):
                scene_path = test
                image = input_image_name
                return scene_path, input_image_name


class Communincation_Module:

    def __init__(self):
        """ Performs system communication through the Requests API"""
        self.port = 8004


    def post_model(self):
        try:
            with open('sherlock.ce', 'r') as sherlock_model:
                model = sherlock_model.read()
            requests.post('http://localhost:' + str(self.port) + '/sentences', data = model)
        except requests.exceptions.RequestException as e:
            error_message = "Error posting model to instance. Please check your connection with the localhost CEServer.\n"
            print error_message
            print e
            sys.exit(0)        


    def post_to_shared_kb(self, tellcard):
        try:
            requests.post("http://explorer.cenode.io:" + "6789" + "/sentences", data = tellcard)
        except requests.exceptions.RequestException as e:
            print "Posting to shared knowledge base failed. Please check your connection with the external instance at http://explorer.cenode.io.\n" 
            print e


    def get_instances(self):
        try:
            response = requests.get('http://localhost:' + str(self.port) + '/instances').json()
            return response            
        except requests.exceptions.RequestException as e:
            print "Unable to get instances. Please check your connection with the localhost CEServer.\n"
            print e
            os._exit(0)

    def get_concepts(self):
        try:
            response = requests.get('http://localhost:' + str(self.port) + '/concepts').json()
            return response            
        except requests.exceptions.RequestException as e:
            print "Unable to get concepts. Please check your connection with the localhost CEServer.\n"
            print e
            os._exit(0)


    def get_model_concept_with_id(self, id):
        try:
            response = requests.get("http://localhost:" + str(self.port) + "/concept" + "?id=" + str(id)).json()
            return response            
        except requests.exceptions.RequestException as e:
            print "Unable to get concept with ID. Please check the concept ID exists and your connection with the localhost CENode exists.\n"
            print e
            os._exit(0)


    def get_model_instance_with_id(self, id):
        try:
            response = requests.get("http://localhost:" + str(self.port) + "/instance" + "?id=" + str(id)).json()
            return response            
        except requests.exceptions.RequestException as e:
            print "Unable to get instance ID. Please check the instance ID exists and your connection with the localhost CENode exists.\n"
            print e
            os._exit(0)


class Linguistic_Module:

    def __init__(self, chat_id):

        self.sherlock_model = Communincation_Module()
        self.instance_data = self.sherlock_model.get_instances()
        self.concept_data = self.sherlock_model.get_concepts()
        self.sift_parameters = self.get_sift_parameters()


        self.bot = telepot.Bot('353119581:AAGUpjclZ2RjWW-L-OrHU4bTHltqB1SGYFc')
        self.chat_id = chat_id
        # 'What colour hat is Elephant wearing?' type question
        self.question_group_1 = ["q4", "q7", "q24", "q41"]
        # "Where is the apple?"" type question
        self.question_group_2 = ["q6", "q8", "q25", "q47", "q48", "q53"]
        # 'Which character is in the emerald room?' type question
        self.question_group_3 = ["q9", "q13", "q37", "q45", "q28"]


    def get_sift_parameters(self):
        """ Gets SIFT parameters relating to game objects"""
        concepts = self.concept_data
        for concept_name in self.concept_data:
            if concept_name["name"] == "sift parameter set":
                concept_id = concept_name["id"]
                self.sift_parameters = self.sherlock_model.get_model_concept_with_id(concept_id)
        return self.sift_parameters


    def check_if_question_exists(self, input_string):
        """ Take question and check if it exists in the model. If it exists, extract 
             useful data. """
        input_string = input_string.lower()
        question_data = self.get_model_questions()
        found = False
        for question in question_data:
            if str(question["values"][0]["targetName"]).lower() == input_string:
                subjectType = question["relationships"][0]["targetConceptName"]
                subjectName = question["relationships"][0]["targetName"]
                relationship = question["values"][1]["targetName"]
                questionID = question["name"]
                found = True

        if found:
            self.bot.sendMessage(self.chat_id, "Valid question found, forwarding for processing.")            
            self.process_question_data(subjectType, subjectName, relationship, questionID)
        else:
            print "Invalid Question"
            self.bot.sendMessage(self.chat_id, "Invalid question, please enter a valid SHERLOCK question.")
            return


    def get_model_questions(self):
        """ gets the questions associated with the sherlock game. """
        question_data = []
        for instance in self.instance_data:
            if instance["conceptName"] == "question":
                question_data.append(self.sherlock_model.get_model_instance_with_id(str(instance["id"])))

        return question_data
        


    def process_question_data(self, subjectType, subjectName, relationship, questionID):
        """ Searches the model for other useful data relating to the question.
            different question formats have a different method of retreiving data.
            These questions ids have been placed in groups in the constructor. """

        objectType = ""

        try:

            # 'What colour hat is Elephant wearing?' type question
            if questionID in self.question_group_1:
                objectType = relationship
                for concepts in self.concept_data:
                    if concepts["name"] == relationship:
                        subjectTypeID = concepts["id"]
                        subjectTypeConcept = self.sherlock_model.get_model_concept_with_id(subjectTypeID)
                        relationship = subjectTypeConcept["relationships"][0]["label"]

                
            # "Where is the apple?"" type question
            elif questionID in self.question_group_2:
                for concepts in self.concept_data:
                    if concepts["name"] == subjectType:
                        concept_id = concepts["id"]

                subjectTypeConcept = self.sherlock_model.get_model_concept_with_id(concept_id)        

                for concepts in subjectTypeConcept["parents"]:
                    if concepts["name"] == "locatable thing":
                        concept_id = concepts["id"]

                subjectTypeConcept = self.sherlock_model.get_model_concept_with_id(concept_id)

                for objectTypeValue in subjectTypeConcept["relationships"]:
                    locatableID = objectTypeValue["targetId"]

                objectTypeConcept = self.sherlock_model.get_model_concept_with_id(locatableID)

                for objectTypeValue in objectTypeConcept["children"]:
                    objectType = objectTypeValue["name"]     


            # 'Which character is in the emerald room?' type question
            elif questionID in self.question_group_3:
                for concepts in self.concept_data:
                    if concepts["name"] == subjectType:
                        subjectTypeID = concepts["id"]

                subjectTypeConcept = self.sherlock_model.get_model_concept_with_id(subjectTypeID)

                for conceptValues in subjectTypeConcept["values"]:
                    if conceptValues["label"] == relationship:
                        objectType = conceptValues["targetName"]
                        objectTypeID = conceptValues["targetId"]
                        break

                for conceptValues in subjectTypeConcept["relationships"]:         
                    if conceptValues["label"] == relationship: 
                        objectType = conceptValues["targetName"]
                        objectTypeID = conceptValues["targetId"]
                        break

                objectTypeConcept = self.sherlock_model.get_model_concept_with_id(objectTypeID)

                for objectTypeValue in objectTypeConcept["parents"]:
                    if objectTypeValue["name"] == "locatable thing":
                        locatableID = objectTypeValue["id"]

                objectTypeConcept = self.sherlock_model.get_model_concept_with_id(locatableID)

                for objectTypeValue in objectTypeConcept["relationships"]:
                    relationship = objectTypeValue["label"]
                    locatableID = objectTypeValue["targetId"]

                objectTypeConcept = self.sherlock_model.get_model_concept_with_id(locatableID)

                for objectTypeValue in objectTypeConcept["children"]:
                    subjectType = objectTypeValue["name"]                    


            # All other questions
            else:
                for concepts in self.concept_data:
                    if concepts["name"] == subjectType:
                        subjectTypeID = concepts["id"]

                subjectTypeConcept = self.sherlock_model.get_model_concept_with_id(subjectTypeID)

                for conceptValues in subjectTypeConcept["relationships"]:
                    if conceptValues["label"] == relationship:
                        objectType = conceptValues["targetName"]
                        break

            self.bot.sendMessage(self.chat_id, "We're looking for {0} associated with '{1}'".format(objectType, subjectName))
            print "We're looking for '{0}' associated with '{1}'".format(objectType, subjectName)

        except:
            print "Could not gather all data from the model"
            tb = traceback.format_exc()
            print tb 
            return


        self.search_environment(objectType, subjectName, subjectType, relationship, questionID)


    def search_environment(self, objectType, subjectName, subjectType, relationship, questionID):
        """ Searches the environment of posters for objects"""

        subject_data = self.get_subjectName_image_path_and_params(subjectName)
        object_data = self.get_object_image_paths_and_params(objectType)            

        image_recognition = Visual_Module(self.chat_id)
        found_object = image_recognition.find_scenes_with_objects(subject_data, object_data)
  

        self.construct_output(subjectType, subjectName, relationship, objectType, found_object, questionID)


    def get_subjectName_image_path_and_params(self, subjectName):
        """ Gets the subjectName image path and get training parameters
            for the image"""

        for index_entry in self.instance_data:
            if index_entry["name"] == subjectName:
                objectID = index_entry["id"]


        instance_id_data = self.sherlock_model.get_model_instance_with_id(objectID)

        subject_image_path = str(instance_id_data["values"][0]["targetName"])
        subject_image_name = str(instance_id_data["name"])

        for sift_obj in self.sift_parameters["instances"]:
            if str(sift_obj["name"]).lower() == subjectName.lower() + " parameters":
                siftID = sift_obj["id"]

        instance_id_data = self.sherlock_model.get_model_instance_with_id(siftID)
        
        for value in instance_id_data["values"]:
            if value["label"] == "maximum distance":
                max_distance = value["targetName"]
            elif value["label"] == "minimum matches":
                min_matches = value["targetName"]

        parameters = (subject_image_path, max_distance, min_matches, subject_image_name)
        return parameters
        

    def get_object_image_paths_and_params(self, objectType):
        """ Gets the objectType image paths and get training parameters
            for each of the images. """

        objectID_list = []
        object_name_list = []
        object_image_path = []
        object_image_name = []
        max_distance = []
        min_matches = []
        parameters = []

        for index_entry in self.instance_data:
            if index_entry["conceptName"] == objectType:
                objectID_list.append(index_entry["id"])
                object_name_list.append(index_entry["name"])

        for id_value in objectID_list:
            instance_id_data = self.sherlock_model.get_model_instance_with_id(id_value)
            object_image_path.append(str(instance_id_data["values"][0]["targetName"]))
            object_image_name.append(str(instance_id_data["name"]))

        for obj_name in object_name_list:
            for sift_obj in self.sift_parameters["instances"]:
                if str(sift_obj["name"]).lower() == obj_name.lower() + " parameters":
                    siftID = sift_obj["id"]

            instance_id_data = self.sherlock_model.get_model_instance_with_id(siftID)

            for value in instance_id_data["values"]:
                if value["label"] == "maximum distance":
                    max_distance.append(value["targetName"])
                elif value["label"] == "minimum matches":
                    min_matches.append(value["targetName"])

        for x, y, z, n in zip(object_image_path, max_distance, min_matches, object_image_name):
            parameters.append((x,y,z,n))

        return parameters


    def construct_output(self, subjectType, subjectName, relationship, objectType, found_object, questionID):
        """ Prints the output to the bot and to terminal """
        if questionID in self.question_group_1 or questionID in self.question_group_3:
            message_to_output = "The {3} \\'{4}\\' {2} the {0} \\'{1}\\'".format(subjectType, subjectName, relationship, objectType, found_object)
            message_to_print = "The {3} '{4}' {2} the {0} '{1}'".format(subjectType, subjectName, relationship, objectType, found_object)
        else:
            message_to_output = "The {0} \\'{1}\\' {2} the {3} \\'{4}\\'".format(subjectType, subjectName, relationship, objectType, found_object)
            message_to_print = "The {0} '{1}' {2} the {3} '{4}'".format(subjectType, subjectName, relationship, objectType, found_object)


        print "CNL: {0}".format(message_to_print)
        self.bot.sendMessage(self.chat_id, message_to_print)


        agentName = "sherlock"
        botName = "sherBot"
        tellcard = """there is a tell card named 'msg_{{uid}}' that has '{0}' as content and is to the agent '{1}' and is from the agent '{2}' and has the timestamp '{{now}}' as timestamp""".format(message_to_output, agentName, botName)

        self.sherlock_model.post_to_shared_kb(tellcard)

        final_message = "Observation posted to shared KB."
        print final_message
        self.bot.sendMessage(self.chat_id, final_message)


if __name__ == "__main__":
    Communincation_Module().post_model()
    go = telegram_handle()
    print "AI Player Launched!"
    go.run()

