import numpy as np
import cv2
from matplotlib import pyplot as plt
from os import listdir
from os.path import isfile, join

def main(query_image, train_image):
    # This variable should be dynamic based on the train image
    MIN_MATCH_COUNT = 25

    img1 = cv2.imread(query_image,0)          # queryImage
    img2 = cv2.imread(train_image,0) # trainImage

    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    matches = flann.knnMatch(des1,des2,k=2)

    # store all the good matches as per Lowe's ratio test.
    good = []
    distances = []
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)
            #print m.distance

    # print sorted(distances, key=float)

    if len(good)>MIN_MATCH_COUNT:
        match_array = [matches.distance for matches in good]
        
        print "query_image: {0} \tvs\t train_image: {1}\t\t (matches, distance) = ({2}, {3})".format(query_image, train_image, len(good), sum(match_array)/len(match_array))

        

if __name__ == "__main__":

    test_scene = [f for f in listdir('./scenes') if isfile(join('./scenes', f))]  
    # train_scene = 'received_scenes/test_image_orange.jpg'

    # train_scene = [f for f in listdir('./objects') if isfile(join('./objects', f))]
    train_scene = ["alt_apple.jpg", "apple.jpg", "alt_apple_2.jpg"]
    
    for train in train_scene:
        for test in test_scene:
            main("scenes/" + test, "objects/" + train)