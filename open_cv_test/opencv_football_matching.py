#!/usr/bin/python

import numpy as np
import cv2
from matplotlib import pyplot as plt

def main(test, train):

    print "Testing Image: {0}".format(test)
    print "Training Image: {0}".format(train)


    img1 = cv2.imread(test,0) # queryImage
    img2 = cv2.imread(train,0) # trainImage

    # Initiate SIFT detector
    orb = cv2.ORB_create(scoreType=cv2.ORB_FAST_SCORE,WTA_K = 3)

    # find the keypoints and descriptors with SIFT
    kp1, des1 = orb.detectAndCompute(img1,None)
    kp2, des2 = orb.detectAndCompute(img2,None)

    # create BFMatcher object
    bf = cv2.BFMatcher(cv2.NORM_HAMMING2, crossCheck=True)

    # Match descriptors.
    matches = bf.match(des1,des2)

    match_sum = []

    for match in matches:
        # print match.distance
        match_sum.append(match.distance)

    matching_sum = np.sum(match_sum)
    matching_num = len(match_sum)

    #print matching_num
    # print matching_sum

    # Sort them in the order of their distance.

    matches = sorted(matches, key = lambda x:x.distance)

    max_dist = 0.0
    match_array = []

    for match in matches[:20]:
        match_array.append(match.distance)

    print sum(match_array) / 20

    

    # Draw first 10 matches.
    #img3 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,flags=2)
    img3 = cv2.drawMatches(img1,kp1,img2,kp2,matches[:10],None, flags=2)

    #plt.imshow(img3)
    #plt.show()

    #if matching_sum > 5000.0:
        #print "No Game Object Recognised."



if __name__ == "__main__":

    trainScene = ['Scenes/elephant_orange.JPG', 'Scenes/elephant_orange_room.JPG', 'Scenes/elephant_tennis_greenhat.JPG', 'Scenes/giraffe_banana_redhat.JPG',
                 'Scenes/giraffe_room_redhat_robot.JPG', 'Scenes/giraffe_rugby.JPG', 'Scenes/hippo_pineapple.JPG', 'Scenes/hippo_room_bluehat.JPG', 'Scenes/hippo_soccer.JPG',
                 'Scenes/lion_golf.JPG', 'Scenes/lion_pinkhat.JPG', 'Scenes/lion_room_lemon_ape.JPG', 'Scenes/tiger_basketball_yellowhat.JPG',
                 'Scenes/tiger_pear.JPG', 'Scenes/tiger_pear_green_baloon.JPG', 'Scenes/zebra_apple_purplehat.JPG', 'Scenes/zebra_apple_room_ghost.JPG', 
                 'Scenes/zebra_cricket.JPG'
                 ]
    testScene = 'Scenes_old/lion_test_image.JPG'

    for train in trainScene:
        main(testScene, train)
