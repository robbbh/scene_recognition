#!/usr/bin/python
import sys
import numpy as np
import cv2

def main():

    try:
        img = cv2.imread('football.jpg',0)

        cv2.imshow('football',img)
        cv2.waitKey(0)
        cv2.imwrite('footballgray.png',img)
        cv2.destroyAllWindows()

    except:
        print "No image found."


if __name__ == "__main__":
    main()
